import Vue from 'vue'
import VueResource from 'vue-resource'
window.Vue = Vue;
Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector("meta[name=csrf_token]").getAttribute('value');;


import App from './App.vue'

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App }
})
