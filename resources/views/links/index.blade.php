@extends('layout')

@section('content')
<div class="put-the-dang-thing-in-the-middle">
    <h1>Available Links</h1>
    
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th>Link</th>
                    <th>URL</th>
                </tr>
            </thead>
            <tbody>
                @foreach($links as $link)
                    <tr>
                        <td>{{ env('BASE_URL', 'http://madewithmagmod/').$link->hash}}</td>
                        <td>{{$link->url}}</td>
                    </tr>
                @endforeach
            </tbody>           

        </table>
    </div>
</div>
@stop
