@extends('layout')

@section('content')
<div>
    <app inline-template>
        <div class="wrapper">
            <div class="put-the-dang-thing-in-the-middle">
                <h1>Shorten a URL</h1>

                {!! Form::open(['url' => 'links']) !!}
                    <div class="row" style=" margin: 0 auto">

                        <div class="form-group col-sm-12">
                        {!! Form::text('url', null, ['class' => 'form-control','v-model' => 'url', 'id' => 'shorten-input', 'placeholder' => 'Enter URL to link to']) !!}
                        {!! $errors->first('url', '<div class="error">:message</div>') !!}
                        </div>
                    </div>
                   
                    <div class="row" style=" margin: 1em auto .5em;">
                    <h4 class="col-sm-12" >Optional tracking modifiers <small>Parameters will be added to the url in the correct format</small></h4>
                    </div>
                    
                    <div class="row" style=" margin: 0 auto">
                        <div class="form-group col-sm-4">
                            {!! Form::text('source', null, ['class' => 'form-control', 'v-model' => 'source', 'placeholder' => 'Who is this link for?']) !!}
                            {!! $errors->first('source', '<div class="error">:message</div>') !!}
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::text('medium', null, ['class' => 'form-control', 'v-model' => 'medium', 'placeholder' => 'What is the medium type? e.g. email, ad, post etc']) !!}
                            {!! $errors->first('medium', '<div class="error">:message</div>') !!}
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::text('campaign', null, ['class' => 'form-control', 'v-model' => 'campaign', 'placeholder' => 'What campaign is this for?']) !!}
                            {!! $errors->first('campaign', '<div class="error">:message</div>') !!}
                        </div>

                    </div>
                    <div class="row" style="margin: 0 auto;">
                        <h5 class="col-sm-12">URL Preview</h5>
                    </div>
                    <div class="row" style="margin: 0 auto;">
                        <div class="col-sm-12 text-center">
                            <em>@{{ previewLink }}</em>
                        </div>
                    </div>
                    <div class="row" style=" margin: 0 auto">
                        <div class="col-sm-2 pull-right text-right">
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary'])  !!}
                        </div>
                    </div>
                {!! Form::close() !!}

                @if (Session::has('hashed'))
                    <output>{!! link_to(Session::get('hashed')) !!}</output>
                @endif
            </div>
        </div>

        <div class="available">
            <h3>Available links</h3>
            <v-table 
                :data.sync="tableData"
                :columns="gridColumns"
                type="quantity">
            </v-table>
            
        </div>
    </app>
</div>
<script type="text/javascript">
var MagMod = {
    baseURL: '{!! env('BASE_URL', 'http://madewithmagmod/') !!}',
    links: {!! json_encode($links)  !!}
}
</script>
@stop
