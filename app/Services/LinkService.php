<?php

namespace MagMod\Services;

use MagMod\Link;
use MagMod\Utilities\UrlHasher;
use MagMod\Exceptions\NonExistentHashException;

class LinkService {

	 protected $urlHasher;

    /**
     * @param UrlHasher $urlHasher
     */
    public function __construct(UrlHasher $urlHasher)
    {
        $this->urlHasher = $urlHasher;
    }

  /**
     * Save url to db and hash
     *
     * @param $url
     *
     * @return string
     */
    public function make($url, $params = array())
    {
        $link = Link::byUrl($url);

        return $link ? $link->hash : $this->makeHash($url, $params);
    }

    /**
     * Fetch a url by hash
     *
     * @param $hash
     *
     * @return mixed
     * @throws \MagMod\Exceptions\NonExistentHashException
     */
    public function getUrlByHash($hash)
    {
        $link = Link::byHash($hash);

        if ( ! $link) throw new NonExistentHashException;

        return $link->url;
    }

    /**
     * Prepare and save new url + hash
     *
     * @param $url
     * @returns string
     */
    private function makeHash($url, $params = array())
    {
        $hash = $this->urlHasher->make($url);
        $data = compact('url', 'hash');
        $data = array_merge($data, $params);
        \Event::fire('link.creating', [$data]);

        Link::create($data);

        return $hash;
    }

}