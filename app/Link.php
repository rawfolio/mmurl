<?php

namespace MagMod;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['url', 'hash', 'source', 'campaign', 'medium'];

     /**
     * Fetch link by hash
     *
     * @param $hash
     *
     * @return mixed
     */
    public static function byHash($hash)
    {
        return Link::whereHash($hash)->first();
    }

    /**
     * Fetch link by url
     *
     * @param $url
     *
     * @return mixed
     */
    public static function byUrl($url)
    {
        return Link::whereUrl($url)->first();
    }

}
