<?php

namespace MagMod\Http\Controllers;

use Illuminate\Http\Request;

use MagMod\Http\Requests;
use MagMod\Http\Controllers\Controller;
use MagMod\Exceptions\NonExistentHashException;
use MagMod\Exceptions\ValidationException;
use MagMod\Link;
use Little;

class LinksController extends Controller
{
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
         $links = Link::orderBy('id', 'desc')->get();
        return view('links.create', compact('links'));
    }

    public function show() 
    {
        $links = Link::all();
        return view('links.index', compact('links'));
    }

    public function showJson() 
    {
        $links = Link::all();
        return response()->json($links);//view('links.index', compact('links'));
    }

    public function update(Link $link, Request $request)
    {
        $url = $request->get('url');
        if($url && $link->url != $url) {
            $link->url = $url;
            $link->save();
        }
        return response()->json($link);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try
        {
            $url = $request->get('url');
            $params = $request->only(['source', 'campaign', 'medium']);
            $params = array_filter($params);
            $query = '';
            $queryParams = [];

            $urlParsed = parse_url($url);

            // if(isset($urlParsed['query'])) {
            //     parse_str($urlParsed['query'], $queryParams);
            // }

            foreach($params as $key => $val) {
                $queryParams['utm_'.$key] = $val;
            }

            if($queryParams) {
                $query = http_build_query($queryParams);
            }

            
            
            if($query) {
                if(isset($urlParsed['query'])) {
                    $url = sprintf('%s&%s', $url, $query);
                } else {
                    $url = sprintf('%s?%s', $url, $query);
                }
            }

            $hash = Little::make($url, $params);
        }

        catch (ValidationException $e)
        {
            return back()->withErrors($e->getErrors())->withInput();
        }

        return back()->with([
            'flash_message' => 'Here you go! ' . link_to($hash),
            'hashed'        => $hash
        ]);
    }

    /**
     * Accept hash, and fetch url
     *
     * @param $hash
     *
     * @return mixed
     */
    public function processHash($hash)
    {
        try
        {
            $url = Little::getUrlByHash($hash);

            return redirect($url, 301);
        }

        catch (NonExistentHashException $e)
        {
            return redirect()->route('home')->with('flash_message', 'Sorry - could not find your desired URL.');
        }
    }

    public function destroy(Link $link)
    {
        $link->delete();
        return response(null, 204);
    }
}
