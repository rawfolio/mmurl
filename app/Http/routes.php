<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function() {
	
	// $referrer = Request::server('HTTP_REFERER');
	// $referrer = $referrer ? Request::server('HTTP_REFERER') : 'Direct';
	// $tracking = '?utm_source='.$referrer.'&utm_campaign=PeoplesChoice2015';
	//$url = 'https://magnetmod.com/madewithmagmod'; //.$tracking;
	$url = env('DEFAULT_ROUTE', 'https://magnetmod.com/madewithmagmod');
	return redirect($url, 301); 
});

Route::get('/2015', function() {
	
	$referrer = Request::server('HTTP_REFERER');
	$referrer = $referrer ? Request::server('HTTP_REFERER') : 'Direct';
	//$tracking = '?utm_source='.$referrer.'&utm_campaign=PeoplesChoice2015';
	$url = 'https://magnetmod.com/thecontest2015?utm_source='.$referrer.'&utm_campaign=PeoplesChoice2015'; //.$tracking;
	return redirect($url, 301); 
});


Route::get('/bf', function() {
	return redirect('/weeNU', 301); 
});

Route::get('/jobs', function() {
	$url = 'https://www.glassdoor.com/Overview/Working-at-MagMod-EI_IE1763396.11,17.htm';
	return redirect($url, 301); 
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/create', ['as' => 'home', 'uses' => 'LinksController@create']);	
    Route::get('/links', ['as' => 'links', 'uses' => 'LinksController@show']);	
    Route::get('/alllinks', ['as' => 'linksjson', 'uses' => 'LinksController@showJson']);	
	Route::post('links', 'LinksController@store');
	Route::post('links/{link}', 'LinksController@update');
	Route::delete('links/{link}', 'LinksController@destroy');
	
});

Route::get('{hash}', 'LinksController@processHash');