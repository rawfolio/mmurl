<?php

namespace MagMod\Providers;

use Illuminate\Support\ServiceProvider;
use MagMod\Utilities\UrlHasher;
class UtilitiesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('MagMod\Utilities\UrlHasher', function()
        {
            $length = 5;

            return new UrlHasher($length);
        });
    }
}
